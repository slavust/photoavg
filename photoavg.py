#!/usr/bin/python3
from PIL import Image
import os

images = []

for item in os.listdir('.'):
	if os.path.isfile(item):
		try:
			img = Image.open(item)
			images.append(img.convert('RGB'))
		except Exception as e:
			continue

sz = images[0].size

result = Image.new('RGB', sz, color='black')
result_pixels = result.load()

for x in range(sz[0]):
	for y in range(sz[1]):
		sum = [0, 0, 0]
		for im in images:
			rgb = im.getpixel((x, y))
			for i in range(3):
				sum[i] += rgb[i]

		avg_color = [int(c / len(images)) for c in sum]
		result_pixels[x, y] = tuple(avg_color)

result.save('avg.png')
